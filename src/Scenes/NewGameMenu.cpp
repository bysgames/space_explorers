#include <SE/Scenes/NewGameMenu.hpp>
#include <SE/Scenes/Game.hpp>
#include <SE/Entities/Spacecraft.hpp>
#include <SE/Assets.h>
#include <cmath>
#include <BYS/Core/Logger.hpp>
#include <BYS/UI/Button.hpp>
using namespace bys;

////////////////////////////////////////
NewGameMenu::NewGameMenu(Director &director) : Scene(director)
{
    setName("NewGameMenu");
    setCursorTexture("textures/cursor.png");

    auto title = new Text("Choose your spacecraft", "fonts/dark_forest/darkforest.ttf", 100);
    title->setOriginCenter();
    title->setPosition(683, 100);
    title->setColor(Color("green"));
    addChild(title);

    auto button = new Button(this);
    button->setCSS("css/NewGameMenu.css", "prevShipButton");
    button->onLeftClick.connect(std::bind(&NewGameMenu::moveShip, this, -1));
    addChild(button);

    button = new Button(this);
    button->setCSS("css/NewGameMenu.css", "nextShipButton");
    button->onLeftClick.connect(std::bind(&NewGameMenu::moveShip, this, 1));
    addChild(button);

    auto spaceship = new Sprite;
    spaceship->setName("spacecraft");
    spaceship->setPosition(683, 400);
    addChild(spaceship);

    auto text = new Text("", "fonts/dark_forest/darkforest.ttf", 30);
    text->setName("spacecraftName");
    text->setOriginCenter();
    text->setPosition(683, 560);
    text->setColor(Color("green"));
    addChild(text);

    button = new Button(this);
    button->setCSS("css/NewGameMenu.css", "backButton");
    button->onLeftClick.connect(std::bind(&Scene::close, this, 0));
    addChild(button);

    button = new Button(this);
    button->setCSS("css/NewGameMenu.css", "playButton");
    button->onLeftClick.connect(std::bind(&Scene::close, this, 1));
    addChild(button);

    spacecrafts.emplace_back("textures/ships/fighter-01.png", "fighter-01");
    spacecrafts.emplace_back("textures/ships/rsH6n.png", "rsH6n");
    spacecrafts.emplace_back("textures/ships/alienspaceship.png", "alienship");
    spacecrafts.emplace_back("textures/ships/redfighter-06.png", "redfighter-06");
    spacecrafts.emplace_back("textures/ships/bluedeath.png", "bluedeath");
    setShip(0);
}

////////////////////////////////////////
void NewGameMenu::onClose(int scene)
{
    switch (scene)
    {
        case 0:
            directorAction.type = DirectorAction::PopScene;
        break;

        case 1:
            if (! createSystem())
                printf("Error creating system\n");

            directorAction.type = DirectorAction::PushScene;
            directorAction.scene = new Game(*getDirector(), (Spacecraft::Type) currentSpacecraft, "solar system");
        break;
    }
}

///////////////////////////////////////
void NewGameMenu::onUpdate(float deltaTime)
{
    getChild("spacecraft")->rotate(deltaTime * 15.f);
}

////////////////////////////////////////
void NewGameMenu::moveShip(int direction)
{
    if (currentSpacecraft + direction < 0)
        setShip(spacecrafts.size() - 1);
    else if (currentSpacecraft + direction == spacecrafts.size())
        setShip(0);
    else
        setShip(currentSpacecraft + direction);
}

////////////////////////////////////////
void NewGameMenu::setShip(int index)
{
    currentSpacecraft = index;
    static_cast <Sprite *> (getChild("spacecraft"))->setTexture(spacecrafts [currentSpacecraft].first, true);
    auto spacecraftName = static_cast <Text *> (getChild("spacecraftName"));
    spacecraftName->setString(spacecrafts [currentSpacecraft].second);
    spacecraftName->setOriginCenter();
}

////////////////////////////////////////
bool NewGameMenu::createSystem()
{
    sqlite3 *db;
    int result = sqlite3_open(ASSETS"se.sql", &db);

    if (result != SQLITE_OK)
    {
        sqlite3_close(db);
        Logger::print("NewGameMenu", "Error opening db ?", StringList() << sqlite3_errmsg(db));
        return false;
    }

    ///// Insert into system table
    sf::String systemName("solar system");
    int asteroidBelt = 0;
    sf::String stmt("INSERT INTO system VALUES ('" + systemName + "', " + std::to_string(asteroidBelt) + ");");
    char *errorMessage = nullptr;

    result = sqlite3_exec(db, stmt.toAnsiString().c_str(), nullptr, nullptr, &errorMessage);

    if (result != SQLITE_OK)
    {
        sqlite3_close(db);
        Logger::print("NewGameMenu", "Error inserting system row into db ?", StringList() << errorMessage);
        return false;
    }

    ///// Insert into planet table
    int planetsNumber = rand() % 4 + 7;
    registerPlanet("Sun", Planet::Sun, Vector2D(), db, systemName);

    for (int i = 0; i < planetsNumber; i++)
    {
        float angle = (rand() % 360) * 3.14159265f / 180.f;
        float distance = (1 + i) * 1000;
        registerPlanet("X" + std::to_string(i + 1), Planet::Type(rand() % 4),
                       Vector2D(cos(angle) * distance, sin(angle) * distance), db, systemName);
    }

    sqlite3_close(db);
    return true;
}

bool NewGameMenu::registerPlanet(const sf::String &name, Planet::Type type, const Vector2D &position, sqlite3 *db,
                                 const sf::String &systemName)
{
    sf::String stmt("INSERT INTO planet VALUES ('" + name + "', " + std::to_string(type) + ", " +
                    std::to_string(position.x) + ", " + std::to_string(position.y) + ", '" +
                    systemName + "');");

    char *errorMessage = nullptr;
    int result = sqlite3_exec(db, stmt.toAnsiString().c_str(), nullptr, nullptr, &errorMessage);

    if (result != SQLITE_OK)
    {
        sqlite3_close(db);
        Logger::print("NewGameMenu", "Error inserting system row into db ?", StringList() << errorMessage);
        return false;
    }

    return true;
}
