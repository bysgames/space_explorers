#include <SE/Scenes/MainMenu.hpp>
#include <BYS/UI/Button.hpp>
using namespace bys;

////////////////////////////////////////
MainMenu::MainMenu(Director &director) : Scene(director)
{
    setName("MainMenu");
    setCursorTexture("textures/cursor.png");

    auto title = new Text("Space Explorers", "fonts/dark_forest/darkforest.ttf", 100);
    title->setOriginCenter();
    title->setPosition(683, 100);
    title->setColor(Color("green"));
    addChild(title);

    auto button = new Button(this);
    button->setCSS("css/MainMenu.css", "newGameButton");
    button->onLeftClick.connect(std::bind(&Scene::close, this, 1));
    addChild(button);

    button = new Button(this);
    button->setCSS("css/MainMenu.css", "loadGameButton");
    addChild(button);

    button = new Button(this);
    button->setCSS("css/MainMenu.css", "optionsButton");
    addChild(button);

    button = new Button(this);
    button->setCSS("css/MainMenu.css", "quitButton");
    button->onLeftClick.connect(std::bind(&Scene::close, this, 0));
    addChild(button);
}

////////////////////////////////////////
#include <SE/Scenes/NewGameMenu.hpp>
void MainMenu::onClose(int scene)
{
    switch (scene)
    {
        case 0:
            directorAction.type = DirectorAction::Exit;
        break;

        case 1:
            directorAction.type = DirectorAction::PushScene;
            directorAction.scene = new NewGameMenu(*getDirector());
        break;
    }
}
