#include <SE/Scenes/Game.hpp>
#include <SE/Entities/Asteroid.hpp>
#include <SE/Entities/Planet.hpp>
#include <SE/Assets.h>
#include <BYS/Core/Logger.hpp>
#include <sqlite3.h>
#include <sstream>
using namespace bys;

////////////////////////////////////////
Game::Game(Director &director, Spacecraft::Type spacecraftType, const sf::String &systemName) : Scene(director)
{
    setName("GameScene");

    /////////////////// Player camera
    auto player = new Spacecraft(spacecraftType);
    player->setName("beto");
    player->setScale(0.5f, 0.5f);
    cameraFollow(Scene::DefaultCamera, player);
    addChild(player);

    ///////////////////// HUD camera
    const auto &directorSize = director.getSize();
    int hudCamera = 1;
    addCamera(Camera(Vector2D(), directorSize, hudCamera));

    auto background = new Sprite("textures/space.png");
    background->setCamera(hudCamera);
    addChild(background);

    auto hud = new Sprite("textures/hud/visor.png");
    hud->setZIndex(5);
    hud->setCamera(hudCamera);
    addChild(hud);

    auto minimap = new Minimap(hudCamera, player, this);
    minimap->setZIndex(5);
    minimap->setPosition(- 543, 216);
    addChild(minimap);

    loadSystem(systemName, minimap);

    connectToKeyboard(sf::Keyboard::Escape, std::bind(&Scene::close, this, 0));
}

////////////////////////////////////////
Game::~Game()
{
    getDirector()->setView(getDirector()->getDefaultView());
}

////////////////////////////////////////
void Game::onClose(int scene)
{
    directorAction.type = DirectorAction::PopScene;
}

////////////////////////////////////////
bool Game::loadSystem(const sf::String &systemName, Minimap *minimap)
{
    sqlite3 *db;
    int result = sqlite3_open(ASSETS"se.sql", &db);

    if (result != SQLITE_OK)
    {
        sqlite3_close(db);
        Logger::print("Game", "Error opening db ?", StringList() << sqlite3_errmsg(db));
        return false;
    }

    //// Load system
    sf::String query("SELECT name, type, x_position, y_position FROM planet WHERE system = '" + systemName + "';");
    sqlite3_stmt *stmt;
    result = sqlite3_prepare_v2(db, query.toAnsiString().c_str(), -1, &stmt, nullptr);

    if (result != SQLITE_OK)
    {
        sqlite3_close(db);
        Logger::print("Game", "Error opening db ?", StringList() << sqlite3_errmsg(db));
        return false;
    }

    while (sqlite3_step(stmt) == SQLITE_ROW)
    {
        std::stringstream ss;
        ss << sqlite3_column_text(stmt, 0);
        Planet::Type planetType = Planet::Type(sqlite3_column_int(stmt, 1));
        int xPosition = sqlite3_column_int(stmt, 2);
        int yPosition = sqlite3_column_int(stmt, 3);

        auto planet = new Planet(planetType, ss.str());
        planet->setPosition(xPosition, yPosition);
        addChild(planet);
        minimap->addPlanet(*planet);

        Logger::print("Game", "Added planet");
    }

    sqlite3_close(db);
    return true;
}
