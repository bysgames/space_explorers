#include <SE/Entities/Spacecraft.hpp>
#include <SE/Entities/Gun.hpp>
#include <BYS/Game/Scene.hpp>
#include <SFML/Graphics/View.hpp>
#include <cmath>

////////////////////////////////////////
Spacecraft::Spacecraft(Type type, float aceleration, float minSpeed, float maxSpeed,
                       float rotationSpeed, float brakeSpeed, float friction) :
                       aceleration(aceleration), minSpeed(minSpeed), speed(minSpeed), maxSpeed(maxSpeed + minSpeed),
                       rotationSpeed(rotationSpeed), breakSpeed(brakeSpeed), friction(friction),
                       aceleratorKey(sf::Keyboard::W), breakKey(sf::Keyboard::S), rotateLeftKey(sf::Keyboard::A),
                       rotateRightKey(sf::Keyboard::D), fireKey(sf::Keyboard::Space),
                       type(type)
{
    setZIndex(3);

    auto leftGun = new Gun(0.2f);
    leftGun->move(18, 155);
    addGun(leftGun);

    auto rightGun = new Gun(0.2f);
    rightGun->move(178, 155);
    addGun(rightGun);

    if (type == Fighter01)
    {
        setTexture("textures/ships/fighter-01.png");
    }
    else if (type == rsH6n)
    {
        setTexture("textures/ships/rsH6n.png");
    }
    else if (type == AlienShip)
    {
        setTexture("textures/ships/alienspaceship.png");
    }
    else if (type == RedFighter06)
    {
        setTexture("textures/ships/redfighter-06.png");
    }
    else if (type == Bluedeath)
    {
        setTexture("textures/ships/bluedeath.png");
    }
}

////////////////////////////////////////
void Spacecraft::onUpdate(float deltaTime)
{
    if (sf::Keyboard::isKeyPressed(rotateLeftKey))
        rotate(- deltaTime * rotationSpeed);

    if (sf::Keyboard::isKeyPressed(rotateRightKey))
        rotate(deltaTime * rotationSpeed);

    if (sf::Keyboard::isKeyPressed(aceleratorKey))
    {
        if (speed < maxSpeed)
            speed += aceleration;
    }
    else if (speed > minSpeed)
    {
        speed -= friction;
    }

    if (sf::Keyboard::isKeyPressed(breakKey))
    {
        if (speed == minSpeed)
            speed = 0;
        else if (speed > 0)
            speed -= breakSpeed;
    }
    else if (speed == 0)
    {
        speed = minSpeed;
    }

    auto rotation = (getRotation() - 90.f) * 3.14159265f / 180.f;
    move(speed * deltaTime * cos(rotation), speed * deltaTime * sin(rotation));

    if (sf::Keyboard::isKeyPressed(fireKey))
        shoot();
}

////////////////////////////////////////
void Spacecraft::addGun(Gun *gun)
{
    guns.push_back(gun);
    addChild(gun);
}

////////////////////////////////////////
void Spacecraft::shoot()
{
    for (auto *gun : guns)
        gun->shoot();
}
