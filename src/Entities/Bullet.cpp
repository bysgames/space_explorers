#include <SE/Entities/Bullet.hpp>
#include <cmath>

////////////////////////////////////////
Bullet::Bullet(float rotation) : lifeTime(2.f), speed(2000.f)
{
    setName("bullet");
    setZIndex(100);
    setTexture("textures/guns/bullets.png");
    setTextureRect(Rect(0, 0, 39, 39));
    this->rotation = (rotation - 90.f) * 3.14159265f / 180.f;
}

////////////////////////////////////////
void Bullet::onUpdate(float deltaTime)
{
    lifeTime -= deltaTime;

    if (lifeTime <= 0.f)
    {
        release();
        return;
    }

    move(deltaTime * speed * cos(rotation), deltaTime * speed * sin(rotation));
}
