#include <SE/Entities/Minimap.hpp>
#include <BYS/Game/Sprite.hpp>
#include <BYS/Game/Camera.hpp>

////////////////////////////////////////
Minimap::Minimap(int hudCamera, Entity *player, Scene *scene) : player(player), minimapCamera(2)
{
    setCamera(hudCamera);

    auto background = new Sprite("textures/hud/background.png");
    addChild(background, true);

    center = new Sprite("textures/hud/player.png");
    addChild(center, true);

    scene->addCamera(Camera(player->getPosition(), scene->getDirector()->getSize(), minimapCamera, "minimapCamera"));
    scene->getCamera(minimapCamera)->setViewport(sf::FloatRect(0.02384f, 0.7026f, 0.1573f, 0.1573f));
    scene->getCamera(minimapCamera)->zoom(3);
    scene->cameraFollow(minimapCamera, player);
}

////////////////////////////////////////
void Minimap::onUpdate(float deltaTime)
{
    center->setRotation(player->getRotation());
}

////////////////////////////////////////
void Minimap::addPlanet(const Planet &planet)
{
    auto minimapPlanet = new Sprite(planet.getTexture());
    minimapPlanet->setPosition(planet.getPosition());
    minimapPlanet->setZIndex(planet.getZIndex() + 3);
    minimapPlanet->setCamera(minimapCamera);
    getParent()->addChild(minimapPlanet);
}
