#include <SE/Entities/Planet.hpp>

////////////////////////////////////////
Planet::Planet(Type type, const sf::String &name)
{
    switch (type)
    {
        case Sun:     setTexture("textures/planets/sun.png");       break;
        case Normal:  setTexture("textures/planets/planet3.png");   break;
        case Yellow:  setTexture("textures/planets/planet1.png");   break;
        case Water:   setTexture("textures/planets/planet2.png");   break;
        case Dark:    setTexture("textures/planets/planet14.png");  break;
        case Moon:    setTexture("textures/planets/moon.png");
    }

    setZIndex(1);
    setName(name);
}
