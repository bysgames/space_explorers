#include <SE/Entities/Asteroid.hpp>
#include <BYS/Actions/SpriteAnimation.hpp>

////////////////////////////////////////
Asteroid::Asteroid()
{
    setZIndex(2);
    SpriteAnimation animation(TimeAction::infinite, 1.f / 25.f);
    animation.addFrames(4, 8, Vector2D(256, 256));
    actions.pushAction(animation);
    actions.play();

    setTexture("textures/asteroids/asteroid2.png");
    setTextureRect(Rect(0, 0, 256, 256));
}
