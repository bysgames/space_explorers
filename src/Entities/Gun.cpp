#include <SE/Entities/Gun.hpp>
#include <SE/Entities/Bullet.hpp>

////////////////////////////////////////
Gun::Gun(float firerate) : firerate(firerate), timeSinceLastShoot(firerate)
{
}

////////////////////////////////////////
void Gun::shoot()
{
    if (timeSinceLastShoot < firerate)
        return;

    auto parent = getParent();
    auto bullet = new Bullet(parent->getRotation());
    bullet->setPosition(getFullPosition());
    getRoot()->addChild(bullet);
    timeSinceLastShoot = 0.f;
}

////////////////////////////////////////
void Gun::onUpdate(float deltaTime)
{
    if (timeSinceLastShoot < firerate)
        timeSinceLastShoot += deltaTime;
}
