#include <BYS/Game/AssetManager.hpp>
#include <BYS/Game/Director.hpp>
#include <SE/Assets.h>
#include <SE/Scenes/MainMenu.hpp>
#include <cstdlib>
using namespace bys;

int main()
{
    srand(time(nullptr));

    AssetManager::assetsPath = ASSETS;
    auto &director = Director::getInstance();
    director.create(sf::VideoMode::getDesktopMode(), "Space Explorers", sf::Style::Fullscreen);
    director.setFramerateLimit(60);
    return director.run(new MainMenu(director));
}
