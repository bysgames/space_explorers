#ifndef BYS_GAMES_SPACE_EXPLORERS_PLANET_HPP
#define BYS_GAMES_SPACE_EXPLORERS_PLANET_HPP

#include <BYS/Game/Sprite.hpp>
using namespace bys;

class Planet : public Sprite
{
public:
    enum Type
    {
        Normal = 0,
        Yellow = 1,
        Water  = 2,
        Dark   = 3,
        Sun    = 4,
        Moon   = 5
    };

    Planet(Type type, const sf::String &name);
};


#endif // BYS_GAMES_SPACE_EXPLORERS_PLANET_HPP
