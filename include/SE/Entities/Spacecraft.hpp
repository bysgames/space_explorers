#ifndef BYS_GAMES_SPACE_EXPLORERS_SPACECRAFT_HPP
#define BYS_GAMES_SPACE_EXPLORERS_SPACECRAFT_HPP

#include <SFML/Window/Keyboard.hpp>
#include <BYS/Game/Sprite.hpp>
#include <vector>
using namespace bys;

class Gun;

class Spacecraft : public Sprite
{
public:
    enum Type
    {
        Fighter01    = 0,
        rsH6n        = 1,
        AlienShip    = 2,
        RedFighter06 = 3,
        Bluedeath    = 4
    };

    Spacecraft(Type type = Fighter01, float aceleration = 10.f, float minSpeed = 20.f,
               float maxSpeed = 1000.f, float rotationSpeed = 75.f, float brakeSpeed = 10.f, float friction = 5.f);
    virtual void onUpdate(float deltatTime);

    void addGun(Gun *gun);
    void shoot();

private:
    Type type;

    /// Engine
    float aceleration;
    float speed;
    float minSpeed;
    float maxSpeed;
    float rotationSpeed;
    float friction;
    float breakSpeed;

    sf::Keyboard::Key aceleratorKey;
    sf::Keyboard::Key breakKey;
    sf::Keyboard::Key rotateLeftKey;
    sf::Keyboard::Key rotateRightKey;
    sf::Keyboard::Key fireKey;

    /// Guns
    std::vector <Gun *> guns;

    Entity *hud;
    Entity *background;
};

#endif // BYS_GAMES_SPACE_EXPLORERS_SPACECRAFT_HPP
