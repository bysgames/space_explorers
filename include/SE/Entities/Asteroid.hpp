#ifndef BYS_GAMES_SPACE_EXPLORERS_ASTEROID_HPP
#define BYS_GAMES_SPACE_EXPLORERS_ASTEROID_HPP

#include <BYS/Game/Sprite.hpp>
using namespace bys;

class Asteroid : public Sprite
{
public:
    Asteroid();
};

#endif // BYS_GAMES_SPACE_EXPLORERS_ASTEROID_HPP
