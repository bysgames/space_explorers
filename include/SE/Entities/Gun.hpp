#ifndef BYS_GAMES_SPACE_EXPLORERS_GUN_HPP
#define BYS_GAMES_SPACE_EXPLORERS_GUN_HPP

#include <BYS/Game/Entity.hpp>
using namespace bys;

class Gun : public Entity
{
public:
    Gun(float firerate = 0.f);
    void shoot();
    virtual void onUpdate(float deltaTime);

private:
    float firerate;
    float timeSinceLastShoot;
};

#endif // BYS_GAMES_SPACE_EXPLORERS_GUN_HPP
