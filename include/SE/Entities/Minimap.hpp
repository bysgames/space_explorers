#ifndef BYS_GAMES_SPACE_EXPLORERS_MINIMAP_HPP
#define BYS_GAMES_SPACE_EXPLORERS_MINIMAP_HPP

#include <SE/Entities/Planet.hpp>
#include <BYS/Game/Entity.hpp>
#include <BYS/Game/Scene.hpp>
using namespace bys;

class Minimap : public Entity
{
public:
    Minimap(int hudCamera, Entity *player, Scene *scene);
    virtual void onUpdate(float deltatTime);
    void addPlanet(const Planet &planet);

private:
    Entity *player;
    Entity *center;
    int minimapCamera;
};

#endif // BYS_GAMES_SPACE_EXPLORERS_MINIMAP_HPP
