#ifndef BYS_GAMES_SPACE_EXPLORERS_BULLET_HPP
#define BYS_GAMES_SPACE_EXPLORERS_BULLET_HPP

#include <BYS/Game/Sprite.hpp>
using namespace bys;

class Bullet : public Sprite
{
public:
    Bullet(float rotation);
    virtual void onUpdate(float deltaTime);

private:
    float speed;
    float rotation;
    float lifeTime;
};

#endif // BYS_GAMES_SPACE_EXPLORERS_BULLET_HPP
