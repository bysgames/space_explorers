#ifndef BYS_GAMES_SPACE_EXPLORERS_NEW_GAME_MENU_HPP
#define BYS_GAMES_SPACE_EXPLORERS_NEW_GAME_MENU_HPP

#include <BYS/Game/Scene.hpp>
#include <SE/Entities/Planet.hpp>
#include <string>
#include <utility>
#include <vector>
#include <sqlite3.h>
using namespace bys;

class NewGameMenu : public Scene
{
public:
    NewGameMenu(Director &director);
    virtual void onClose(int scene = 0);
    virtual void onUpdate(float deltaTime);

private:
    void moveShip(int direction);
    void setShip(int index);

    bool createSystem();
    bool registerPlanet(const sf::String &name, Planet::Type type, const Vector2D &position, sqlite3 *db,
                        const sf::String &systemName);

    std::vector <std::pair <std::string, std::string>> spacecrafts;
    int currentSpacecraft;
};

#endif // BYS_GAMES_SPACE_EXPLORERS_NEW_GAME_MENU_HPP
