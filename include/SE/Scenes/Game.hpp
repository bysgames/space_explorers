#ifndef BYS_GAMES_SPACE_EXPLORERS_GAME_HPP
#define BYS_GAMES_SPACE_EXPLORERS_GAME_HPP

#include <BYS/Game/Scene.hpp>
using namespace bys;

#include <SE/Entities/Spacecraft.hpp>
#include <SE/Entities/Minimap.hpp>

class Game : public Scene
{
public:
    Game(Director &director, Spacecraft::Type spacecraftType, const sf::String &systemName);
    ~Game();
    virtual void onClose(int scene = 0);

private:
    bool loadSystem(const sf::String &systemName, Minimap *minimap);
};

#endif // BYS_GAMES_SPACE_EXPLORERS_GAME_HPP
